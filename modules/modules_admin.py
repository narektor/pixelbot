# Pixelbot
# Admin command module
# © 2022 Narek

from telegram.ext import Updater, CommandHandler
import telegram
from .functions import *

def ban(update, context):
	# admin protection
	if not adminGate(update, context) and not pmGate(update):
		return
	# get target
	if update.message.reply_to_message:
		target = update.message.reply_to_message.from_user
	else:
		update.message.reply_text(text="Reply to someone to ban them!")
		return
	# self kick protection
	if not selfGate(update, context, target.id, "ban"):
		return

	update.effective_chat.ban_member(target.id)
	update.message.reply_text(text=f"{target.first_name} was banned!")

def unban(update, context):
	# admin protection
	if not adminGate(update, context) and not pmGate(update):
		return
	# get target
	if update.message.reply_to_message:
		target = update.message.reply_to_message.from_user
	else:
		update.message.reply_text(text="Reply to someone to unban them!")
		return
	# self kick protection
	if not selfGate(update, context, target.id, "unban"):
		return

	update.effective_chat.unban_member(target.id)
	update.message.reply_text(text=f"{target.first_name} was unbanned!")

def kick(update, context):
	# admin protection
	if not adminGate(update, context) and not pmGate(update):
		return
	# get target
	if update.message.reply_to_message:
		target = update.message.reply_to_message.from_user
	else:
		update.message.reply_text(text="Reply to someone to kick them!")
		return
	# self kick protection
	if not selfGate(update, context, target.id, "kick"):
		return

	update.effective_chat.unban_member(target.id)
	update.message.reply_text(text=f"{target.first_name} was kicked!")

CONTENTS=[
	CommandHandler('ban', ban, run_async=True),
	CommandHandler('unban', unban, run_async=True),
	CommandHandler('pardon', unban, run_async=True),
	CommandHandler('kick', kick, run_async=True)
]

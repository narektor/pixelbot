# Pixelbot
# Notes module
# © 2022 Narek

from telegram.ext import Updater, CommandHandler, ChatMemberHandler
import telegram
from telegram import Chat
from json.decoder import JSONDecodeError
import json
import uuid
from pysqlitecipher import sqlitewrapper as sw
from os.path import exists

try:
    from getch import getch
    def getpass(prompt):
        """Replacement for getpass.getpass() which prints asterisks for each character typed"""
        print(prompt, end='', flush=True)
        buf = b''
        while True:
            ch = getch().encode()
            if ch in {b'\n', b'\r', b'\r\n'}:
                print('')
                break
            elif ch == b'\x03': # Ctrl+C
                # raise KeyboardInterrupt
                return ''
            elif ch in {b'\x08', b'\x7f'}: # Backspace
                buf = buf[:-1]
                print(f'\r{(len(prompt)+len(buf)+1)*" "}\r{prompt}{"*" * len(buf)}', end='', flush=True)
            else:
                buf += ch
                print('*', end='', flush=True)
        return buf.decode(encoding='utf-8')
except ImportError:
    from getpass import getpass

if not exists("notes"):
	print("There is no notes database - either it was deleted or this is the first time that you're running Pixelbot. Please enter a password for the new notes database. Remember it or write it down in a safe place - you'll need to enter it whenever Pixelbot starts.")

obj = sw.SqliteCipher(dataBasePath="notes", checkSameThread=False, password=getpass("Password for note database: "))

def int_to_bytes(x: int) -> bytes:
    if x < 0:
        x = -x * 10
    return x.to_bytes((x.bit_length() + 7) // 8, 'big')

def all(update, context):
    chat = update.effective_chat
    id = hash(chat.id)
    if not obj.checkTableExist2(id):
        obj.createTable(id, [["name","TEXT"],["value","TEXT"]], makeSecure=True, commit=True)
    notes = obj.getDataFromTable(id, raiseConversionError=True, omitID=False)
    if len(notes) < 2:
        update.message.reply_text("This chat does not have any notes.")
        return
    try:
        sindex = ""
        for i in notes[1]:
            sindex = f"{sindex}\n\\- `{i[1]}`"
        update.message.reply_markdown_v2("This chat has these notes saved:\n" + sindex)
    except JSONDecodeError:
        update.message.reply_text("Failed to load notes.")

def get(update, context):
    if len(context.args[0]) < 2:
        update.message.reply_text("Please specify the note's name.")
        return
    notename = context.args[0]
    chat = update.effective_chat
    id = hash(chat.id)
    notes = obj.getDataFromTable(id, raiseConversionError=True, omitID=False)
    if len(notes) < 2:
        update.message.reply_text("This chat does not have notes.")
        return
    sent = False
    for c in notes[1]:
        if c[1] == notename:
            sent = True
            update.message.reply_markdown_v2(unformat(c[2]))
    if not sent:
        update.message.reply_text("Note does not exist.")

def save(update, context):
    if len(context.args[0]) < 2:
        update.message.reply_text("Please specify the note's name.")
        return
    notename = context.args[0]
    chat = update.effective_chat
    id = hash(chat.id)
    if not obj.checkTableExist2(id):
        obj.createTable(id, [["name","TEXT"],["value","TEXT"]], makeSecure=True, commit=True)
    msg = format(update.message.reply_to_message)
    saved = False
    notes = obj.getDataFromTable(id, raiseConversionError=True, omitID=False)
    for c in notes[1]:
        if c[1] == notename:
            saved = True
            obj.updateInTable(id, c[0], "value", msg)
    if not saved:
        obj.insertIntoTable(id, [notename, msg], commit=True)
    update.message.reply_markdown_v2(f"Saved note `{notename}`\\.")

def format(original):
    fmt = original.text.replace("\n","\\n").replace("'","''").replace('"','""')
    pe = original.parse_entities(types=[telegram.MessageEntity.BOLD, telegram.MessageEntity.CODE, telegram.MessageEntity.ITALIC, telegram.MessageEntity.STRIKETHROUGH, telegram.MessageEntity.UNDERLINE])
    fmt = parse_markdown(fmt, pe)
    return fmt

# from github.com/userbot8895/HUB-Plus/blob/master/src/notes.py#L48, modified and adapted for markdown v2
def parse_markdown(message, entities):
    parsed = message
    goffset = 0
    for e in entities:
        offset = e.offset + goffset
        length = offset+e.length
        if e.type == telegram.MessageEntity.BOLD:
            parsed = parsed[0:offset]+"*"+parsed[offset:length]+"*"+parsed[length:len(parsed)]
            goffset = goffset + 2
        elif e.type == telegram.MessageEntity.ITALIC:
            parsed = parsed[0:offset]+"_"+parsed[offset:length]+"_"+parsed[length:len(parsed)]
            goffset = goffset + 2
        elif e.type == telegram.MessageEntity.CODE:
            parsed = parsed[0:offset]+"`"+parsed[offset:length]+"`"+parsed[length:len(parsed)]
            goffset = goffset + 2
        elif e.type == telegram.MessageEntity.STRIKETHROUGH:
            parsed = parsed[0:offset]+"~"+parsed[offset:length]+"~"+parsed[length:len(parsed)]
            goffset = goffset + 2
    return parsed

def unformat(formatted):
    fmt = formatted.replace("\\n","\n").replace("''","'").replace('""','"')
    fmt = fmt.replace("-","\\-").replace(".","\\.")
    return fmt

CONTENTS=[
	CommandHandler('get', get, run_async=True),
	CommandHandler('save', save, run_async=True),
	CommandHandler('notes', all, run_async=True),
]

# Pixelbot
# Discussion themes module
# © 2022 Narek

from telegram.ext import Updater, CommandHandler, ChatMemberHandler
import telegram
from telegram import Chat
import requests
import random

topic_url = "https://gitlab.com/narektor/pixelbot-topics/-/raw/main/topics.md"
topics = []

def topic(update, context):
    global topics
    if len(topics) == 0:
        with open("topics.txt","r") as file:
            topics = file.read().split("\n")
    update.message.reply_text(text=f"Let's talk about *{random.choice(topics)}*", parse_mode=telegram.ParseMode.MARKDOWN)

def ntopic(update, context):
    mes = update.message.reply_text(text="Downloading discussion topics...")
    global topics
    topics = dltopics(update)
    mes.edit_text(text="Discussion topics have been downloaded!")

def dltopics(update):
    topics = []
    with open("topics.txt","w") as file:
        r = requests.get(topic_url).text
        for line in r.split("\n"):
            if len(line) == 0 or line.startswith("#"):
                continue
            file.write(line + "\n")
            topics.append(line)
    return topics

CONTENTS=[
	CommandHandler('discuss', topic, run_async=True),
	CommandHandler('newthemes', ntopic, run_async=True)
]

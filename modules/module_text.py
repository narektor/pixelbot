# Pixelbot
# Text for all modules
# © 2022 Narek

import datetime

ETA_ASKING = [
	"Flooding group chats...",
	"Starting time machine...",
	"Spamming XDA-General...",
	"Spamming the dev...",
	"Going to an oracle...",
	"Activating machine learning...",
	"Brute forcing password..."
]

ROOT = """
Check your device's XDA forum for detailed rooting instructions, but the general procedure is:
- Unlock bootloader
- Download Magisk APK from github, rename it to a ZIP
- Flash TWRP
- Flash Magisk inside TWRP
- Reboot phone
- Profit

Or, in case TWRP isn't available:
- Unlock bootloader
- Download Magisk APK from github
- Install the app to patch your device's boot.img (you can find it from your ROM's ZIP)
- Flash that patched boot.img

Bam... enjoy rooted phone
"""

ROOT_SAMSUNG = """
Check your device's XDA forum for detailed rooting instructions, but the general procedure (for Samsung devices) is:
- Unlock bootloader
- Download Magisk APK from github, rename it to a ZIP
- Flash TWRP
- Flash Magisk inside TWRP
- Reboot phone
- Profit

Or, in case TWRP isn't available (PC required):
- Unlock bootloader
- Download Magisk APK from github
- Install the app to patch your device's boot.img
- Repack the patched boot.img to boot.img.tar
- Flash that patched boot.img.tar in Odin

Bam... enjoy rooted phone

(Alternatively, if you have nobody to send you your device's boot.img, download your device's stock firmware and patch the whole AP file)
"""

AB = """
A/B system updates use two sets of partitions referred to as slots (normally slot A and slot B). The system runs from the current slot while the partitions in the unused slot are not accessed by the running system during normal operation. This approach makes updates fault resistant by keeping the unused slot as a fallback: if an error occurs during or immediately after an update, the system can rollback to the old slot and continue to have a working system.

Each slot has a bootable attribute that states whether the slot contains a correct system from which the device can boot. The current slot is bootable when the system is running, but the other slot may have an old (still correct) version of the system, a newer version, or invalid data. Regardless of what the current slot is, there is one slot which is active (the one the bootloader will boot from on the next boot) or the preferred slot.

Each slot also has a successful attribute set by the OS, which is relevant only if the slot is also bootable. A successful slot should be able to boot, run, and update itself.
"""

FLASH = """
*How to flash with the Android Flash Tool*

_NOTE: This guide is for Pixel phones only. For other devices a guide is just a Google search away._

\- Connect your device directly to your PC (no hubs, adapters, extenders, or monitors).
\- Open flash.android.com in a browser. It opens to the Welcome page.
\- Allow the flash tool to communicate with your device through ADB by accepting the popup that says "Allow site access to your ADB keys in order to communicate with devices".
\- Click Add new device.
\- Select your device from the list and click Connect. This list may not contain the full device name.
\- On your device’s screen, select Always allow from this computer and click OK to accept the USB debugging connection.
\- Select the connected device in your browser.
\- Search for and select your desired build from the list. You can also select options, such as wiping the device or force flashing all partitions.
\- Click Install to start the process. The device reboots and enters fastboot mode.
\- After Flash Complete appears, disconnect the device from the USB cable.
"""

SLAP = [
	"a Pixel 5",
	"a Pixel 6",
	"a Pixel 6 Pro",
	"a Samsung",
	"a Pixel 4 bezel",
	"a Galaxy S20 FE",
	"One UI 3",
	"a Galaxy A02s",
	"an iPhone",
	"end of support",
	"an incomplete API",
	"an Exynos 990",
	"a Xiaomi",
	"a Huawei",
	"a Vivo",
	"the Death Star",
	"Android 11",
	"a Redmi",
	"RetardOS",
	"PE for sunfish",
	"bootloader unlock",
	"a potion of retardation",
	"SafetyNet failing",
	"basic attestation",
	"a Galaxy A03s",
	"a brick",
	"official TWRP",
	"bootloops",
	"an ETA request",
	"Magisk",
	"a piece of wood",
	"KG prenormal",
	"Android 12 beta",
	"Android 13 beta",
	"Android 11 for Mi A3",
	"a free 1000% legit no virus free download link",
	"a permanently locked bootloader",
	"HWC fix",
	"a cracked APK",
	"Android 12 beta",
	"sudo",
	"an official pixel room",
	"a DeX port",
	"a Nokia 3310",
	"Material You toggle switches",
	"a Xiaomeme"
]

SLAPPING = [
	"{0} grabs {2} and throws it at {1}.",
	"{0} gets stick bugged by {1} lol.",
	"{0} takes {2} and throws it at {1}.",
	"{0} takes {2} and slaps {1} with it.",
	"{0} grabs {2} and chucks it at {1}.",
	"{0} takes {2} and sends it in {1}'s face.",
	"{0} launches {2} in {1}'s general direction."
	"{0} starts slapping {1} with {2}.",
	"{0} repeatedly slaps {1} with {2}.",
	"{0} repeatedly throws {2} at {1}.",
	"{0} flings {2} at {1}."
]

PUNCH = [
	"{0} gets ran over with a bulldozer.",
	"{0} gets split in half with an axe.",
	"{0} updates his Mi A3 to Android 11.",
	"{0} gets lit up like dynamite.",
	"{0} switches to a Galaxy A02s.",
	"{0} drinks a potion of retardation.",
	"{0} gets stick bugged lol.",
	"{0} gets rickrolled.",
	"{0} gets tased to death.",
	"{0} gets shot with a nail gun.",
	"{0} gets squished with an anvil.",
	"A million Galaxy Homes get thrown at {0}. They survived.",
	"{0} gets dissected alive.",
	"{0} gets shred into dust.",
	"{0} was murdered. *FATALITY*",
	"{0} gets thrown into a volcano.",
	"{0} becomes a retard.",
	"{0} gets locked up in jail."
]

MANAGERS = [
	"MissRose_bot",
	"GroupHelpBot",
	"GroupHelpOfficialCloneBot",
	"IzumiUchihaBOT",
	"PawneeGoddess_Bot",
	"CalsiBot",
	"lindahotbot",
	"corsicanu_bot",
	"SayaAman_bot",
	"kigyorobot",
	"TheRealShadyBot",
	"combot"
]

# Reserved list names.
# This is mostly done to prevent news lists impersonating Pixelbot owners from being created.
RESERVED_LISTS = [
	"admin",
	"officialnews",
	"official",
	"official_news",
	"owner",
	"pixelbot_owner",
	"bot_owner",
	"master",
	"pixelbot_master",
	"bot_master",
	"pixel_bot",
	"pixelbot",
	"mafia",
	"administrators",
	"news_channel",
	"nwes",
	"new",
	"update",
	"updates",
	"officialupdate",
	"officialupdates",
	"official_update",
	"official_updates",
	"broadcast"
]

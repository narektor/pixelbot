# Pixelbot
# Helper functions
# © 2022 Narek

from telegram.ext import Updater, CommandHandler
import telegram

PX_TABLE = {
	"lock":"🔒"
}

# Check if the sender or bot is an administrator, and reply with an error if not.
# Returns false if the sender or bot isn't an administrator, otherwise true.
def adminGate(update, context, checkBot = False):
	bot = context.bot.get_chat_member(update.message.chat.id, context.bot.id)
	sender = context.bot.get_chat_member(update.message.chat.id, update.message.from_user.id)
	if sender.status != "administrator" and sender.status != "creator":
		update.message.reply_text(text="You can't use this because you're not an admin.")
		return False
	# Skip bot admin check if requested
	if not checkBot:
		return True
	if bot.status != "administrator" and bot.status != "creator":
		update.message.reply_text(text="You can't use this because I'm not an admin.")
		return False
	return True

# Check if the target is the bot itself, and reply with an error if so.
# Returns false if the target is the bot itself, otherwise true.
def selfGate(update, context, target, action):
	if target == context.bot.id:
		_alt = f"Attempt to use {context.bot.name} to {action} {context.bot.name} unsuccessful."
		update.message.reply_text(text=f"I can't {action} myself.")
		return False
	if target == update.message.from_user.id:
		update.message.reply_text(text=f"You can't {action} yourself.")
		return False
	return True

# Check if the message was sent to a private chat, and reply with an error if so.
# Returns false if the message was sent to a private chat, otherwise true.
def pmGate(update):
	if update.message.chat.type == "private":
		update.message.reply_text(text="This command can't be used in private chats.")
		return False
	return True

# Adjusts px[] notations in text according to the px table.
def px(text):
	newtext = text
	# Remove empty keys
	newtext = newtext.replace("px[]", "")
	for key in PX_TABLE:
		newtext = newtext.replace(f"px[{key}]", PX_TABLE[key])
	return newtext

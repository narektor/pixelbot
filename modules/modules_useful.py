# Pixelbot
# Useful command module
# © 2022 Narek

from .module_text import *
from telegram.ext import Updater, CommandHandler
import telegram
from androlib import asb
from bs4 import BeautifulSoup
import requests
from config import PIRACY
import os.path
import os
import hashlib

# BUF_SIZE is totally arbitrary, change for your app!
BUF_SIZE = 5207

ADB_ERROR = """
There is no single /adb command.

Use:
/adbL for Linux
/adbM for Mac
/adbW for Windows
"""

def root(update, context):
	update.message.reply_text(text=ROOT, parse_mode=telegram.ParseMode.MARKDOWN)

def root_sammy(update, context):
	update.message.reply_text(text=ROOT_SAMSUNG, parse_mode=telegram.ParseMode.MARKDOWN)

def ab(update, context):
	update.message.reply_text(text=AB, parse_mode=telegram.ParseMode.MARKDOWN)

def fastboot(update, context):
	update.message.reply_text(text=FLASH, parse_mode=telegram.ParseMode.MARKDOWN)

def modules(update, context):
	f = open("magisk.txt","r")
	update.message.reply_text(text=f.read(), parse_mode=telegram.ParseMode.MARKDOWN_V2)
	f.close()

def bulletin(update, context):
	data = asb.getLatestASB()
	spl = ""
	if len(data["spl"][0]) == 1:
		spl = data["spl"]
	else:
		for l in data["spl"]:
			spl = f"{spl},{l}"
		spl = spl.lstrip(",")
	crunch = f'*Latest Android Security Bulletin - {data["date"]}*\n*Published on:* {data["publishedOn"]}\n*SPL(s):* {spl}\n[Read more]({data["url"]})'
	update.message.reply_text(text=crunch, parse_mode=telegram.ParseMode.MARKDOWN)

def bulletin_pixel(update, context):
	data = asb.getLatestPixelSB()
	spl = ""
	if len(data["spl"][0]) == 1:
		spl = data["spl"]
	else:
		for l in data["spl"]:
			spl = f"{spl},{l}"
		spl = spl.lstrip(",")
	crunch = f'*Latest Pixel Update Bulletin - {data["date"]}*\n*Published on:* {data["publishedOn"]}\n*SPL(s):* {spl}\n[Read more]({data["url"]})'
	update.message.reply_text(text=crunch, parse_mode=telegram.ParseMode.MARKDOWN)

def odin(update, context):
	ms = update.message.reply_text(text="_Downloading Odin..._", parse_mode=telegram.ParseMode.MARKDOWN)
	x = requests.get("https://odindownload.com/download/")
	soup = BeautifulSoup(x.content, 'html.parser')
	dl = soup.select("#page > a:nth-child(9)")
	url = dl[0].get("href")
	fn = "freezer/"+url.split("/")[len(url.split("/"))-1]
	if not path.exists(fn):
		print(f"New Odin version {fn.replace('freezer/','').replace('.apk','')}!")
		ms.edit_text(text=f"Found new Odin version `{fn.replace('freezer/','').replace('.apk','')}`!\n_Downloading..._", parse_mode=telegram.ParseMode.MARKDOWN)
		print("Downloading...", end='')
		r = requests.get(url, allow_redirects=True)
		print(" done.\nWriting...", end='')
		open(fn, 'wb').write(r.content)
		print(" done.")
	ms.edit_text(text="_Uploading ZIP...\nThis might take a while. You will get a new reply when it's done._", parse_mode=telegram.ParseMode.MARKDOWN)
	update.message.reply_document(document=open(fn, 'rb'), caption="The latest version of Odin.", timeout=60, parse_mode=telegram.ParseMode.MARKDOWN)
	ms.delete()

def getPT(update, context, platform):
	url = f"https://dl.google.com/android/repository/platform-tools-latest-{platform}.zip"
	human_platform = platform
	if human_platform == "darwin":
		human_platform = "mac"
	human_platform = human_platform[0].upper() + human_platform[1:]
	ms = update.message.reply_text(text=f"_Downloading adb for {human_platform}..._", parse_mode=telegram.ParseMode.MARKDOWN)
	fn = "freezer/"+url.split("/")[len(url.split("/"))-1]
	nfn = fn[:-4] + "-temp.zip"
	if path.exists(nfn):
		os.delete(nfn)
	print(f"Downloading {platform} platform-tools")
	ms.edit_text(text=f"_Downloading adb for {human_platform}..._", parse_mode=telegram.ParseMode.MARKDOWN)
	print("Downloading...", end='')
	r = requests.get(url, allow_redirects=True)
	print(" done.\nWriting...", end='')
	open(nfn, 'wb').write(r.content)
	print(" done.\n...", end='')
	ms.edit_text(text=f"_Comparing..._", parse_mode=telegram.ParseMode.MARKDOWN)
	nhash = hash(nfn)
	ohash = hash(fn)
	if nhash is not ohash:
		os.delete(fn)
		os.rename(nfn, fn)
	os.delete(nfn)
	print(" done.")
	ms.edit_text(text="_Uploading ZIP...\nThis might take a while. You will get a new reply when it's done._", parse_mode=telegram.ParseMode.MARKDOWN)
	update.message.reply_document(document=open(fn, 'rb'), caption=f"The latest platform-tools for {human_platform}. Includes adb, fastboot and some other tools.", timeout=60, parse_mode=telegram.ParseMode.MARKDOWN)
	ms.delete()

def getPTwin(update, context):
	getPT(update, context, "windows")

def getPTlnx(update, context):
	getPT(update, context, "linux")

def getPTmac(update, context):
	getPT(update, context, "darwin")

def adbHelp(update, context):
	update.message.reply_text(text=ADB_ERROR)

def hash(fi):
	sha1 = hashlib.sha1()
	with open(fi, 'rb') as f:
		while True:
			data = f.read(BUF_SIZE)
			if not data:
				break
		sha1.update(data)
	return sha1.hexdigest()

CONTENTS = [
	CommandHandler('root', root, run_async=True),
	CommandHandler('rootsam', root_sammy, run_async=True),
	CommandHandler('ab', ab, run_async=True),
	CommandHandler('flash', fastboot, run_async=True),
	CommandHandler('modules', modules, run_async=True),
	CommandHandler('asb', bulletin, run_async=True),
	CommandHandler('psb', bulletin_pixel, run_async=True),
	CommandHandler('odin', odin, run_async=True),
	CommandHandler('adbW', getPTwin, run_async=True),
	CommandHandler('adbL', getPTlnx, run_async=True),
	CommandHandler('adbM', getPTmac, run_async=True),
	CommandHandler('adb', adbHelp, run_async=True)
]

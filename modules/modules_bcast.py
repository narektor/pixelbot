# Pixelbot
# Broadcast module
# © 2022 Narek

from telegram.ext import Updater, CommandHandler, ChatMemberHandler
import telegram
from pysqlitecipher import sqlitewrapper as sw
from telegram import Chat
from .module_text import RESERVED_LISTS
from os.path import exists
from .functions import *

try:
    from getch import getch
    def getpass(prompt):
        """Replacement for getpass.getpass() which prints asterisks for each character typed"""
        print(prompt, end='', flush=True)
        buf = b''
        while True:
            ch = getch().encode()
            if ch in {b'\n', b'\r', b'\r\n'}:
                print('')
                break
            elif ch == b'\x03': # Ctrl+C
                # raise KeyboardInterrupt
                return ''
            elif ch in {b'\x08', b'\x7f'}: # Backspace
                buf = buf[:-1]
                print(f'\r{(len(prompt)+len(buf)+1)*" "}\r{prompt}{"*" * len(buf)}', end='', flush=True)
            else:
                buf += ch
                print('*', end='', flush=True)
        return buf.decode(encoding='utf-8')
except ImportError:
    from getpass import getpass

news_help = """
Usage:

/news create <list name> - creates a news list.
/news join <list name> - adds a group to a news list. Announcements from the news list will be forwarded to the group.
/news leave <list name> - removes a group from a news list.
/news announce <list name> - announces something in a news list. Announcements will be forwarded to all groups that joined the list. You can only announce in lists that you own.
/news a <list name>, /announce <list name> - shorthands for above command.
"""

if not exists("news"):
	print("There is no news database - either it was deleted or this is the first time that you're running Pixelbot. Please enter a password for the new news database. Remember it or write it down in a safe place - you'll need to enter it whenever Pixelbot starts.")
obj = sw.SqliteCipher(dataBasePath="news", checkSameThread=False, password=getpass("Password for news database: "))

def news(update, context):
    command = ""
    if len(context.args) > 0:
        command = context.args[0]
    if command == "create":
        news_create(update, context)
    elif command in ["announce", "a"]:
        news_announce(update, context)
    elif command == "join":
        news_join(update, context)
    elif command == "leave":
        news_leave(update, context)
    else:
        update.message.reply_text(text=news_help)

def news_create(update, context):
    if len(context.args) < 2:
        update.message.reply_text(text=news_help)
        return
    listname = context.args[1].lower()
    # list names are unique!
    if obj.checkTableExist(listname) or listname in RESERVED_LISTS:
        update.message.reply_text("A list with this name already exists.")
        return
    obj.createTable(listname, [["id","TEXT"]], makeSecure=True, commit=True)
    # 1st object is always the owner's ID
    obj.insertIntoTable(listname, [update.message.from_user.id], commit=True)
    update.message.reply_text(f"Created news list {listname}.")

def news_announce(update, context):
    if len(context.args) < 2:
        update.message.reply_text(text=news_help)
        return
    listname = context.args[1].lower()
    # list names are unique!
    if not obj.checkTableExist(listname):
        update.message.reply_text(f"The list '{listname}' doesn't exist. You can create it with /news create {listname}.")
        return
    # 1st object is always the owner's ID
    groups = obj.getDataFromTable(listname, raiseConversionError=True, omitID=True)[1]
    owner = groups[0][0]
    if str(update.message.from_user.id) != owner:
        update.message.reply_text(f"You are not the owner of the news list '{listname}'.")
        return
    if not update.message.reply_to_message:
        update.message.reply_text(f"Please reply to a message to announce it.")
        return
    # load group list
    status_msg = update.message.reply_text(f"Announcing to '{listname}'...")
    for lst in groups:
        id = lst[0]
        if id == owner or id == str(update.message.chat.id):
            continue
        msg = update.effective_message.reply_to_message.forward(chat_id=id)
        msg.reply_text(f"You received this message because this group is subscribed to the '{listname}' news list.")
    status_msg.edit_text(f"✅ Announced to groups.")

# Adds a group to a news list.
def news_join(update, context):
    if len(context.args) < 2:
        update.message.reply_text(text=news_help)
        return
    if not pmGate(update) or not adminGate(update, context, False):
        return
    listname = context.args[1].lower()
    # list names are unique!
    if not obj.checkTableExist(listname):
        update.message.reply_text(f"The list '{listname}' doesn't exist. You can create it with /news create {listname}.")
        return
    obj.insertIntoTable(listname, [update.message.chat.id], commit=True)
    update.message.reply_text(f"Added this group to list {listname}.")

# Removes a group from a news list.
def news_leave(update, context):
    if len(context.args) < 2:
        update.message.reply_text(text=news_help)
        return
    if not pmGate(update) or not adminGate(update, context, False):
        return
    listname = context.args[1].lower()
    # list names are unique!
    if not obj.checkTableExist(listname):
        update.message.reply_text(f"The list '{listname}' doesn't exist.")
        return
    groups = obj.getDataFromTable(listname, raiseConversionError=True, omitID=True)[1]
    for i in range(len(groups)):
        id = groups[i][0]
        if id == str(update.message.chat.id):
            obj.deleteDataInTable(listname, i, commit=True, raiseError=True, updateId=True)
            update.message.reply_text(f"Removed this group from list {listname}.")
            return
    update.message.reply_text(f"This group isn't in list {listname}.")

CONTENTS=[
	CommandHandler('news', news, run_async=True),
	CommandHandler('announce', news_announce, run_async=True)
]

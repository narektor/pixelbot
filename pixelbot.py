# Pixelbot
# Main file
# © 2022 Narek

# Import libraries
from telegram.ext import Updater, CommandHandler
import telegram
import logging
# Import the command packs, to use the commands in them
from modules import modules_joke, modules_useful, modules_admin, modules_topics, modules_notes, modules_bcast
# Import px functions
from modules.functions import px, PX_TABLE
# Import config
from config import TOKEN
import config

VERSION = "13"
FORM = "https://forms.gle/HMy57F8zqHwKHzfb6"

HELLO = """
Hello!

I'm Pixelbot, a bot created by @thegreatporg based on ideas from @pixel2020_official. The main topic of me is phones.
I'm in development. You can suggest commands for me with /suggest.
I'm also open source. Check the source code with /source.
If you want to see news about me join the news channel: @pxb_news.
Last but not least, you can support my creators by checking out /donate.
"""

CREDITS = """
@thegreatporg - developer
{}
@tthalheim - /root and /rootsam
@pixel2020_official - /magisk
"""

DONATION = """
Want to support the people behind Pixelbot? That's nice :3

Support the developer of Pixelbot on [ko-fi](https://ko-fi.com/thegreatporg).
Support the person that provides hosting for Pixelbot on [PayPal.me](https://paypal.me/jeykul).{}
"""
# Command code
def start(update, context):
	update.message.reply_text(text=HELLO)

def changes(update, context):
	f = open("changes.txt", "r")
	text = px(f.read())
	update.message.reply_text(text=text, parse_mode=telegram.ParseMode.MARKDOWN_V2)
	f.close()

def help_cmd(update, context):
	f = open("help.txt", "r")
	text = f.read()
	update.message.reply_text(text=text, parse_mode=telegram.ParseMode.MARKDOWN_V2)
	f.close()

def credits(update, context):
	cred = CREDITS
	if config.EXTRA_CREDITS:
		cred = cred.format('\n'.join(config.EXTRA_CREDITS))
	else:
		cred = cred.format("")
	update.message.reply_text(text=cred)

def oss(update, context):
	update.message.reply_text(text="Click [here](https://gitlab.com/narektor/pixelbot/) to see my source!", parse_mode=telegram.ParseMode.MARKDOWN)

def feats(update, context):
	update.message.reply_text(text=f"To suggest features, topics and helpful Magisk modules (for /modules) fill and submit [this]({FORM}) form.", parse_mode=telegram.ParseMode.MARKDOWN)

def donate(update, context):
	don = DONATION
	if config.DONATION_URLS:
		don = don.format(f"\nAnd, support the maintainers of this fork: {', '.join(config.DONATION_URLS)}.")
	else:
		don = don.format("")
	update.message.reply_text(text=don, parse_mode=telegram.ParseMode.MARKDOWN)

# development command
def pxTable(update, context):
	text = ""
	for pxk in PX_TABLE:
		text += f"{pxk} is '{PX_TABLE[pxk]}'\n"
	update.message.reply_text(text="MD:\n"+text, parse_mode=telegram.ParseMode.MARKDOWN)
	update.message.reply_text(text="MDv2:\n"+text, parse_mode=telegram.ParseMode.MARKDOWN_V2)

# Create updater and dispatcher 
updater = Updater(token=TOKEN)
dispatcher = updater.dispatcher
# Set up logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
# Add handlers for built in commands
dispatcher.add_handler(CommandHandler('start', start, run_async=True))
dispatcher.add_handler(CommandHandler('help', help_cmd, run_async=True))
dispatcher.add_handler(CommandHandler('changes', changes, run_async=True))
dispatcher.add_handler(CommandHandler('credits', credits, run_async=True))
dispatcher.add_handler(CommandHandler('source', oss, run_async=True))
dispatcher.add_handler(CommandHandler('suggest', feats, run_async=True))
dispatcher.add_handler(CommandHandler('donate', donate, run_async=True))
# Add command handlers
print("Importing commands")
ch = modules_useful.CONTENTS + modules_joke.CONTENTS + modules_topics.CONTENTS + modules_admin.CONTENTS + modules_notes.CONTENTS + modules_bcast.CONTENTS
for h in ch:
	dispatcher.add_handler(h)
# Print a message
print(f"Pixelbot {VERSION} started!")
# Update
updater.start_polling()

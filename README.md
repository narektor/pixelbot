# Pixelbot

A Telegram bot created by members of the [@Pixel2020_official](https://t.me/pixel2020_official) Telegram group. The main topic is phones (specifically Android), with some joke and administration commands.

Can be found on Telegram [here](https://t.me/pixel20_bot).

## Hosting

### Branches

It's recommended to host the [`stable` branch](https://gitlab.com/narektor/pixelbot/-/tree/stable) as it will have the latest stable version of the bot. The [`master` branch](https://gitlab.com/narektor/pixelbot/-/tree/master) has the latest in-development version of Pixelbot, which might be unstable.

### Starting

To start Pixelbot you need to make your own config file. Create a file called `config.py` with the following contents:

```python
TOKEN = "<your bot token from @BotFather>" # bot token, replace this
DONATION_URLS = [] # add donation URLs to show in /donate
```

Then, install the dependencies:
```bash
pip install -r requirements.txt # on Windows
pip3 install -r requirements.txt # on Linux and macOS
```

And finally, start Pixelbot:
```bash
python pixelbot.py # on Windows
python3 pixelbot.py # on Linux and macOS
```

#### If you get an error while using /kill, do this:
```bash
# For Linux:
pip uninstall Pillow
sudo apt-get install libfreetype6-dev # on Ubuntu and Debian, search Google to find the alternative for your distro
pip install Pillow
```

```zsh
# For macOS:
brew remove pil
brew install freetype
brew install pil
```

### Non-free assets

`resources/cheese.mp4` is required for /cheese to work. You can get it by downloading [this](https://www.youtube.com/watch?v=y3qrHn0WALs) video and saving it as `cheese.mp4` in the `resources` folder.
